
public class Aufgabe_III {

	public static void main(String[] args) 
	{
		//AB2 Aufgabe III
		double F1 = -20; double C1 = -28.8889;
		double F2 = -10; double C2 = -23.3333;
		double F3 = 0; double C3 = -17.7778;
		double F4 = 20; double C4 = -6.6667;
		double F5 = 30; double	 C5 = -1.1111;
		
		String F = "Fahrenheit";					String C = "Celsius";
		String fill = String.format("%0" + 25 + "d", 0).replace('0', '-');
		String F1R = String.format("%.0f", F1);		String C1R = String.format("%.2f", C1);
		String F2R = String.format("%.0f", F2);		String C2R = String.format("%.2f", C2);
		String F3R = String.format("%.0f", F3);		String C3R = String.format("%.2f", C3);
		String F4R = String.format("%.0f", F4);		String C4R = String.format("%.2f", C4);
		String F5R = String.format("%.0f", F5);		String C5R = String.format("%.2f", C5);
		
		
		System.out.printf("%-12s | %10s \n", F, C);
		System.out.printf("%25s \n", fill);
		System.out.printf("%-12s | %10s \n", F1R, C1R);
		System.out.printf("%-12s | %10s \n", F2R, C2R);
		System.out.printf("%-12s | %10s \n", F3R, C3R);
		System.out.printf("%-12s | %10s \n", F4R, C4R);
		System.out.printf("%-12s | %10s \n", F5R, C5R);

	}

}
