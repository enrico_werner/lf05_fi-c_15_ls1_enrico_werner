// https://@bitbucket.org/oszimt_lf05/ls01.git
public class Program 
{

	public static void main(String[] args) 
	{
		/*
		System.out.println("Call me\n\"OVERLORD!\"\n");

		System.out.println("Kneel in front of me!\n");
		
		int Armee = 560;
		String Heer = "Drachenreitern";
		System.out.println("Mein Heer von " + Heer + " besteht aus " + Armee + " Reitern und Drachen.\n");
		
		System.out.printf("\s\s\s\s\s\s*\r");
		System.out.printf("\s\s\s\s\s***\r");
		System.out.printf("\s\s\s\s*****\r");
		System.out.printf("\s\s\s*******\r");
		System.out.printf("\s\s*********\r");
		System.out.printf("\s***********\r");
		System.out.printf("*************\r");
		System.out.printf("\s\s\s\s\s***\r");
		System.out.printf("\s\s\s\s\s***\r");
	
		double Ichi = 22.4234234;
		double Ni = 111.2222;
		double San = 4.0;
		double Yon = 1000000.551;
		double Go = 97.34;
		
		System.out.printf("\n%.2f\n", Ichi);
		System.out.printf("%.2f\n", Ni);
		System.out.printf("%.2f\n", San);
		System.out.printf("%.2f\n", Yon);
		System.out.printf("%.2f\n", Go);
		
		//AB2 Aufgabe I
		System.out.println("\t\t\s\s\s**");
		System.out.println("\t\t*\s\s\s\s\s\s*");
		System.out.println("\t\t*\s\s\s\s\s\s*");
		System.out.println("\t\t\s\s\s**");
		
		//X	String n = "0!\n1!\n2!\n3!\n4!\n5!";
		//X	String r = "\n1\n1\s*\s2\n1\s*\s2\s*\s3\n1\s*\s2\s*\s3\s*\s4\n1\s*\s2\s*\s3\s*\s4\s*\s5\n";
		
		//X	System.out.printf("\n%-5s", n);  System.out.printf("%19s", r);
		*/
		//AB2 Aufgabe II
		int a = 1;	
		int b = 2;
		int c = 3;
		int d = 4;
		int e = 5;
		
		System.out.printf("%-5s = %-19s = %4s \n", "0!", "", "1");
		
		String IchiR = String.valueOf(a);
		String IchiE = String.valueOf(a);
		
		System.out.printf("%-5s = %-19s = %4s \n", "1!", IchiR, IchiE);
		
		String NiR = (String.valueOf(a) + "\s*\s" + String.valueOf(b));
		String NiE = String.valueOf(a*b);
		
		System.out.printf("%-5s = %-19s = %4s \n", "2!", NiR, NiE);
		
		String SanR = (String.valueOf(a) + "\s*\s" + String.valueOf(b) + "\s*\s" + String.valueOf(c));
		String SanE = String.valueOf(a*b*c);
				
		System.out.printf("%-5s = %-19s = %4s \n", "3!", SanR, SanE);
		
		String YonR = (String.valueOf(a) + "\s*\s" + String.valueOf(b) + "\s*\s" + String.valueOf(c) + "\s*\s" + String.valueOf(d));
		String YonE = String.valueOf(a*b*c*d);
		
		System.out.printf("%-5s = %-19s = %4s \n", "4!", YonR, YonE);
		
		String GoR = (String.valueOf(a) + "\s*\s" + String.valueOf(b) + "\s*\s" + String.valueOf(c) + "\s*\s" + String.valueOf(d) + "\s*\s" + String.valueOf(e));
		String GoE = String.valueOf(a*b*c*d*e);
				
		System.out.printf("%-5s = %-19s = %4s \n", "5!", GoR, GoE);
		
		System.out.print("\n"); 
		/*
		//AB2 Aufgabe III
		double F1 = -20; double C1 = -28.8889;
		double F2 = -10; double C2 = -23.3333;
		double F3 = 0; double C3 = -17.7778;
		double F4 = 20; double C4 = -6.6667;
		double F5 = 30; double	 C5 = -1.1111;
		
		String F = "Fahrenheit";					String C = "Celsius";
		String fill = String.format("%0" + 25 + "d", 0).replace('0', '-');
		String F1R = String.format("%.0f", F1);		String C1R = String.format("%.2f", C1);
		String F2R = String.format("%.0f", F2);		String C2R = String.format("%.2f", C2);
		String F3R = String.format("%.0f", F3);		String C3R = String.format("%.2f", C3);
		String F4R = String.format("%.0f", F4);		String C4R = String.format("%.2f", C4);
		String F5R = String.format("%.0f", F5);		String C5R = String.format("%.2f", C5);
		
		
		System.out.printf("%-12s | %10s \n", F, C);
		System.out.printf("%25s \n", fill);
		System.out.printf("%-12s | %10s \n", F1R, C1R);
		System.out.printf("%-12s | %10s \n", F2R, C2R);
		System.out.printf("%-12s | %10s \n", F3R, C3R);
		System.out.printf("%-12s | %10s \n", F4R, C4R);
		System.out.printf("%-12s | %10s \n", F5R, C5R);
		*/
		
	}
}
