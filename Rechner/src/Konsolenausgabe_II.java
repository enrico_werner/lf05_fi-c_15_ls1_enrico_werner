import java.util.Scanner; 

public class Konsolenausgabe_II 
{
	public static void main(String[] args)
	{
	Scanner myScanner= new Scanner(System.in); 		
	
	System.out.print("Bitte geben Sie ihren Namen ein: ");  		
	String name = myScanner.next(); 
	
	System.out.print("Bitte geben Sie ihr Alter ein: ");
	int alter = myScanner.nextInt();
	
	System.out.print("\n\n\nIhr Name lautet "+name +" und sie sind "+alter +" Jahre alt.");  
	
	myScanner.close();
	}
}
