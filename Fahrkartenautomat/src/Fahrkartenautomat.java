﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {                	
      Scanner tastatur = new Scanner(System.in);	
      double Fahrkartenzahl = fahrkartenbestellungErfassen(tastatur); 
      double d_rueckgabebetrag = fahrkartenBezahlen(Fahrkartenzahl, tastatur);
      fahrkartenAusgeben();
      rueckgeldAusgeben(d_rueckgabebetrag);
    }
    
    public static double fahrkartenbestellungErfassen(Scanner tastatur)
    {
       double Fahrkartenzahl = 0;       
       
       System.out.print("Wie viele Fahrkarten möchten sie?(Maximal 10 Fahrkarten): ");
       Fahrkartenzahl = tastatur.nextDouble();
       
       	if (Fahrkartenzahl > 10 | Fahrkartenzahl < 0)
       	{       	
       			Fahrkartenzahl = 1;
       			System.out.print("Ihre Eingabe ist ungültig, wir der Vorgang wird mit einer Fahrkarte fortgesetzt.\n\n");      
       	}
       	
       return Fahrkartenzahl;
    }
      
    public static double fahrkartenBezahlen(double Fahrkartenzahl,  Scanner tastatur)
    {
       double Fahrkartenkosten = 2.5; 
       double gesFahrkartenkosten;
       double eingezahlterGesamtbetrag;
       double d_restbetrag;
       String s_restbetrag;
       double eingeworfeneMuenze = 0;
       double d_rueckgabebetrag;
       boolean gueltigeEingabe = false;
       
       gesFahrkartenkosten = Fahrkartenkosten * Fahrkartenzahl;
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < gesFahrkartenkosten)
       {
    	   d_restbetrag = gesFahrkartenkosten - eingezahlterGesamtbetrag;    	
    	   s_restbetrag = String.format("%.2f", d_restbetrag);
    	   do 
    	   {	System.out.printf("Noch zu zahlen: " + s_restbetrag+ " Euro\n");
    	   		System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    	   		eingeworfeneMuenze = tastatur.nextDouble();
    	   		if (eingeworfeneMuenze != 2 | eingeworfeneMuenze != 1 | eingeworfeneMuenze != 0.5 | eingeworfeneMuenze != 0.2 | eingeworfeneMuenze != 0.1 | eingeworfeneMuenze != 0.05)
    	   		{
    	   			System.out.print("Bitte werfen sie eine gültige Münze ein!");
    	   		}
    	   		else
    	   		{
    	   			gueltigeEingabe = true;
    	   		}
    	   } while (gueltigeEingabe == false);
    	   eingezahlterGesamtbetrag += eingeworfeneMuenze;
       }
       d_rueckgabebetrag = eingezahlterGesamtbetrag - gesFahrkartenkosten;
       
       tastatur.close();
       return d_rueckgabebetrag;
    }
      
    public static void fahrkartenAusgeben()
    {
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try 
          {
			Thread.sleep(250);
		  } 
          catch (InterruptedException e) 
          {
			// TODO Auto-generated catch block
			e.printStackTrace();
		  }
       }
       System.out.println("\n\n");
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
               "vor Fahrtantritt entwerten zu lassen!\n"+
               "Wir wünschen Ihnen eine gute Fahrt.");
    }
    
    public static void rueckgeldAusgeben(double d_rueckgabebetrag)
    { 	
        String s_rueckgabebetrag;   
       
       if(d_rueckgabebetrag > 0.0)
       {
    	   s_rueckgabebetrag = String.format("%.2f", d_rueckgabebetrag);
    	   System.out.println("Der Rückgabebetrag in Höhe von " + s_rueckgabebetrag + " Euro");
    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

           while(d_rueckgabebetrag >= 2.0) // 2 EURO-Münzen
           {
        	  System.out.println("2 Euro");
	          d_rueckgabebetrag -= 2.0;
           }
           while(d_rueckgabebetrag >= 1.0) // 1 EURO-Münzen
           {
        	  System.out.println("1 Euro");
	          d_rueckgabebetrag -= 1.0;
           }
           while(d_rueckgabebetrag >= 0.5) // 50 CENT-Münzen
           {
        	  System.out.println("50 Cent");
	          d_rueckgabebetrag -= 0.5;
           }
           while(d_rueckgabebetrag >= 0.2) // 20 CENT-Münzen
           {
        	  System.out.println("20 Cent");
 	          d_rueckgabebetrag -= 0.2;
           }
           while(d_rueckgabebetrag >= 0.1) // 10 CENT-Münzen
           {
        	  System.out.println("10 Cent");
	          d_rueckgabebetrag -= 0.1;
           }
           while(d_rueckgabebetrag >= 0.05)// 5 CENT-Münzen
           {
        	  System.out.println("5 Cent");
 	          d_rueckgabebetrag -= 0.05;
           }           
       }
    }
}