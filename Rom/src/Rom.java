import java.util.Scanner;

public class Rom 
{
	public static void main(String[] args)
	{
		Scanner roemisch = new Scanner(System.in);
		int I = 1;
		int V = 5;
		int X = 10;
		int L = 50;
		int C = 100;
		int D = 500;
		int M = 1000;
		int dezimalWert = 0;
		char[] roemischerWert;
		
		
		System.out.print("r�mischer Zahlenwert:");
		roemischerWert = roemisch.next().toCharArray();
		
		for (int i = 0; i < roemischerWert.length; i++ )
		{
			switch(roemischerWert[i])
			{
			case 'I':
				dezimalWert += I;
				break;
			case 'V':
				dezimalWert += V;
				break;
			case 'X':
				dezimalWert += X;
				break;
			case 'L':
				dezimalWert += L;
				break;
			case 'C':
				dezimalWert += C;
				break;
			case 'D':
				dezimalWert += D;
				break;
			case 'M':
				dezimalWert += M;
				break;
			}
		}
		
		System.out.print(dezimalWert);
		
		
		
	}
}