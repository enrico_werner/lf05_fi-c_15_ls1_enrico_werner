import java.util.Scanner;

public class While 
{
	// Aufhabe 5
	public static void main(String[] args) 
	{
		Scanner tastatur = new Scanner(System.in);
		
		
		
		double zinsen = 0;
		int i = 0;
		
		System.out.println("Wieviel Jahre? ");
		int jahre = tastatur.nextInt();
		System.out.println("Wieviele Kapital?");
		double kapital = tastatur.nextDouble();
		System.out.println("Wie hoch ist der Zinsatz?");
		double zinsatz = tastatur.nextDouble();
		
		zinsatz *= 0.01;
		
		while(i < jahre)
		{
			zinsen = kapital * zinsatz;
			kapital += zinsen;
			i++;
		}
		
		System.out.printf("Ihr Kapital betr�gt: " + " %.2f", kapital);
		tastatur.close();
	}

}
