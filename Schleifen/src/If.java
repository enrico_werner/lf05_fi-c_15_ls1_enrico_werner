import java.util.Scanner;

public class If
{

	public static void main(String[] args) 
	{
		Scanner Werte = new Scanner(System.in);	
		
		int uno = Wert_Uno(Werte);
		int duo = Wert_Duo(Werte);
		int tres = Wert_Tres(Werte);
		String auswertung = Vergleich(uno, duo, tres);
		Auswertung(auswertung);
		
	}
	
	public static int Wert_Uno(Scanner Werte)
	{
		int uno;
		System.out.print("Geben sie Wert 1 ein: ");
		uno = Werte.nextInt();
		
		return uno; 
	}
	
	public static int Wert_Duo(Scanner Werte)
	{
		int duo;
		System.out.print("Geben sie Wert 2 ein: ");
		duo = Werte.nextInt();
		
		return duo; 
	}
	
	public static int Wert_Tres(Scanner Werte)
	{
		int tres;
		System.out.print("Geben sie Wert 3 ein: ");
		tres = Werte.nextInt();
		
		Werte.close();
		return tres; 
	}
	
	public static String Vergleich(int uno, int duo, int tres)
	{
		String auswertung = null;
		
		if(uno == duo && duo == tres && uno == tres)
		{
			auswertung = "Zahl 1, Zahl 2 und Zahl 3 sind gleich gro�.";
		}	
		else if(uno == duo || duo == tres || uno == tres) 
		{
			if(uno == tres && uno < duo)
			{
				auswertung = "Zahl 1 und Zahl 3 sind kleiner als Zahl 2 und gleich gro�.";
			}
			else if(uno == tres && uno > duo) 	
			{
				auswertung = "Zahl 1 und Zahl 3 sind gr��er als Zahl 2 und gleich gro�.";
				
			}
			else if(uno == duo && uno < tres)
			{				
				auswertung = "Zahl 1 und Zahl 2 sind kleiner als Zahl 3 und gleich gro�.";
			}
			else if(uno == duo && uno > tres)
			{
				auswertung = "Zahl 1 und Zahl 2 sind gr��er als Zahl 3 und gleich gro�.";				
			}
			else if(duo == tres && uno < duo)
			{				
				auswertung = "Zahl 2 und Zahl 3 sind kleiner als Zahl 1 und gleich gro�.";
			}
			else if(duo == tres && duo > uno)
			{
				auswertung = "Zahl 2 und Zahl 3 sind gr��er als Zahl 1 und gleich gro�.";
			}			
		}
		else if(uno != duo && duo != tres && uno != tres)
		{
			if(tres < duo && uno < tres)
			{							
				auswertung = "Zahl 1 < Zahl 3 < Zahl 2";
			}
			else if(uno > tres && uno < duo)
			{	
				auswertung = "Zahl 3 < Zahl 1 < Zahl 2";
			}
			else if(uno > tres && uno > duo)
			{
				auswertung = "Zahl 3 < Zahl 2 < Zahl 1";
			}
			else if(uno < duo && duo < tres)
			{
				auswertung = "Zahl 1 < Zahl 2 < Zahl 3";
			}
			else if(duo < tres && tres < uno)
			{
				auswertung = "Zahl 2 < Zahl 3 < Zahl 1";
			}
			else if(duo < uno && uno < tres)
			{
				auswertung = "Zahl 2 < Zahl 1 < Zahl 3";
			}
		}
				
		return auswertung;
	}
	
	public static void Auswertung(String auswertung)
	{
		System.out.println(auswertung);
	}
}
