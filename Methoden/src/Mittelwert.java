import java.util.Scanner;

public class Mittelwert //Aufgabe I
{
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		double x = scanner.nextDouble();
		double y = scanner.nextDouble();
		double m;
		
		m = mittelwert(x, y);
		
		ausgabe(x, y, m);
		
		scanner.close();
	}
	
	public static double ausgabe(double x, double y, double m)
	{
		System.out.printf("Der Mittelwert von %.2f und %.2f ist 5.2f", x, y, m);
	}
	
	public static double mittelwert(double x, double y)
	{
		return (x + y) / 2.0;
	}
}