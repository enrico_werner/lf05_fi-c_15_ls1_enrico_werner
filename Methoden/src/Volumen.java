import java.util.Scanner;

public class Volumen 
{
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		System.out.print("W�rfel Seitenl�nge a:");
		int aw = scanner.nextInt();
		
		System.out.print("Quader Seitenl�nge a:");
		int aq = scanner.nextInt();
		System.out.print("Quader Seitenl�nge b:");
		int bq = scanner.nextInt();
		System.out.print("Quader Seitenl�nge c:");
		int cq = scanner.nextInt();
		
		System.out.print("Pyramide Seitenl�nge a:");
		double ap = scanner.nextInt();
		System.out.print("Pyramide h�he h:");
		double h = scanner.nextInt();
		
		System.out.print("Kugel radius r:");
		double r = scanner.nextInt();
		double pi = 3.141;
		
		int Vw;
		int Vq;
		double Vp;
		double Vk;
		
		Vw = Vwuerfel(aw);
		Vq = Vquader(aq, bq, cq);
		Vp = Vpyramide(ap, h);
		Vk = Vkugel(r, pi);
		
		ausgabe(Vw, Vq, Vp, Vk);
		
		scanner.close();
	}	
	
	public static void ausgabe(int Vw, int Vq, double Vp, double Vk)
	{
		System.out.printf("Das Volumen vom W�rfel ist "+Vw+" \n, vom Quader ist "+Vq+" \n, von der Pyramide ist "+Vp+" \n, und von der Kugel ist %.2f", Vk);
	}
	
	public static int Vwuerfel(int aw)
	{
		return (aw*aw*aw);
	}
	public static int Vquader(int aq, int bq, int cq)
	{
		return (aq*bq*cq);
	}
	public static double Vpyramide(double ap, double h)
	{
		return ap * ap * h / 3;
	}
	public static double Vkugel(double r, double pi)
	{
		return 4 / 3 * r * r * r * pi;
	}
}

