import java.util.Scanner;

public class Multiplikation 
{
	public static void main(String[] args)
	{
		Scanner scanner = new Scanner(System.in);
		int x = scanner.nextInt();
		int y = scanner.nextInt();
		int m;
		
		m = multiplikation(x, y);
		
		ausgabe(x, y, m);
		
		scanner.close();
	}	
	
	public static void ausgabe(int x, int y, int m)
	{
		System.out.printf("Das Produkt von "+ x+" und "+ y+" ist "+ m);
	}
	
	public static int multiplikation(int x, int y)
	{
		return (x * y);
	}
}
